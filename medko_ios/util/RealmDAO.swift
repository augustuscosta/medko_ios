//
//  RealmGenericDAO.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//


protocol RealmDAO {
    
    associatedtype T
    
    static func save(object: T)
    
    static func saveAll(objects: [T])
    
    static func getAll() -> [T]?
    
    static func getByID(id: AnyObject) -> T?
}
