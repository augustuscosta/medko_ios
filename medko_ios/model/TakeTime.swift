//
//  TakeTime.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class TakeTime: Object, Mappable {
    
    dynamic var date = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.date <- map["date"]
    }
    
}

extension TakeTime: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: TakeTime) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [TakeTime]) {
        try! realm.write {
            realm.delete(realm.objects(TakeTime.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [TakeTime]? {
        return [TakeTime](realm.objects(TakeTime.self))
    }
    
    static func getByID(id: AnyObject) -> TakeTime? {
        return realm.object(ofType: TakeTime.self, forPrimaryKey: id)
    }

}
