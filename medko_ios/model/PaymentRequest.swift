//
//  PaymentRequest.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentRequest : Mappable {
    
    dynamic var url = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.url <- map["url"]
    }
}
