//
//  User.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class User : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var email = ""
    dynamic var password = ""
    dynamic var passwordConfirmation = ""
    dynamic var valueHomeCare = ""
    dynamic var currentUser = false
    dynamic var avatar = ""
    dynamic var telephone = ""
    dynamic var rating = false
    dynamic var crm = ""
    dynamic var crmImage = ""
    dynamic var proofOfAddesImage = ""
    dynamic var zipCode = ""
    dynamic var address = ""
    dynamic var facebookAccount = false
    dynamic var facebookToken = ""
    dynamic var accountTypeId = 0
    dynamic var active = false
    dynamic var media = 0
    var bankAccounts = List<BankAccount>()
    var specialities = List<Speciality>()
    var examTypes = List<ExamType>()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.email <- map["email"]
        self.password <- map["password"]
        self.passwordConfirmation <- map["password_confirmation"]
        self.valueHomeCare <- map["value_home_care"]
        self.avatar <- map["avatar"]
        self.telephone <- map["telephone"]
        self.rating <- map["rating"]
        self.crm <- map["crm"]
        self.crmImage <- map["crm_image"]
        self.proofOfAddesImage <- map["proof_of_addes_image"]
        self.zipCode <- map["zip_code"]
        self.address <- map["address"]
        self.facebookAccount <- map["facebook_account"]
        self.facebookToken <- map["facebook_token"]
        self.accountTypeId <- map["account_type_id"]
        self.active <- map["active"]
        self.media <- map["media"]
        self.specialities <- map["speciality_attributes"]
        self.examTypes <- map["exam_type_attributes"]
        self.bankAccounts <- map["bank_account_attributes"]
        
    }
}

extension User: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: User) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [User]) { }
    
    static func getCurrentUser() -> User? {
        return realm.objects(User.self).first
    }
    
    static func getAll() -> [User]? {
        return [User](realm.objects(User.self))
    }
    
    static func getByID(id: AnyObject) -> User? {
        return realm.object(ofType: User.self, forPrimaryKey: id)
    }
    
    static func deleteUsers() {
        try! realm.write {
            realm.delete(realm.objects(User.self))
        }
    }
    
    static func getByName(name: String) -> User? {
        return realm.objects(User.self).filter("name == '\(name)'").first
    }
    
}
