//
//  Exam_Type.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class ExamType: Object, Mappable{
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var homeCare = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.homeCare <- map["home_care"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension ExamType: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: ExamType) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [ExamType]) {
        try! realm.write {
            realm.delete(realm.objects(ExamType.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [ExamType]? {
        return [ExamType](realm.objects(ExamType.self).sorted(byProperty: "name"))
    }
    
    static func getByID(id: AnyObject) -> ExamType? {
        return realm.object(ofType: ExamType.self, forPrimaryKey: id)
    }
    
    static func getByName(name: String) -> ExamType? {
        return realm.objects(ExamType.self).filter("name == \(name)").first
    }
}
