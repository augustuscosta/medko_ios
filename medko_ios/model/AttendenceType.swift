//
//  AttendenceType.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class AttendenceType : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
}

extension AttendenceType: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: AttendenceType) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [AttendenceType]) {
        try! realm.write {
            realm.delete(realm.objects(AttendenceType.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [AttendenceType]? {
        return [AttendenceType](realm.objects(AttendenceType.self))
    }
    
    static func getByID(id: AnyObject) -> AttendenceType? {
        return realm.object(ofType: AttendenceType.self, forPrimaryKey: id)
    }
}
