//
//  Classification.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Classification : Object, Mappable {
    
    dynamic var id = 0
    dynamic var attendenceId = 0
    dynamic var value = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.attendenceId <- map["attendence_id"]
        self.value <- map["value"]
    }
}

extension Classification: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Classification) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [Classification]) {
        try! realm.write {
            realm.delete(realm.objects(Classification.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [Classification]? {
        return [Classification](realm.objects(Classification.self))
    }
    
    static func getByID(id: AnyObject) -> Classification? {
        return realm.object(ofType: Classification.self, forPrimaryKey: id)
    }
}
