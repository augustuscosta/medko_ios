//
//  Session.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Session: Object, Mappable {
    
    dynamic var id = 0
    dynamic var email = ""
    dynamic var password = ""
    dynamic var success = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.email <- map["email"]
        self.password <- map["password"]
        self.success <- map["success"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension Session: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Session) {
        try! realm.write {
            realm.delete(realm.objects(Session.self))
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [Session]) { }
    
    static func getAll() -> [Session]? {
        return [Session](realm.objects(Session.self))
    }
    
    static func getByID(id: AnyObject) -> Session? {
        return realm.object(ofType: Session.self, forPrimaryKey: id)
    }
    
    static func clearAllTables() {
        try! realm.write {
            realm.delete(realm.objects(Session.self))
            realm.delete(realm.objects(User.self))
            realm.delete(realm.objects(Attendence.self))
            realm.delete(realm.objects(ExamType.self))
            realm.delete(realm.objects(Speciality.self))
            realm.delete(realm.objects(Place.self))
            realm.delete(realm.objects(SearchAddress.self))
            realm.delete(realm.objects(BankAccount.self))
            realm.delete(realm.objects(TakeTime.self))
            realm.delete(realm.objects(HealthPlan.self))
            realm.delete(realm.objects(Payment.self))
            realm.delete(realm.objects(PaymentMethod.self))
            
            UserDefaults.standard.removeObject(forKey: "accountTypeId")
        }
    }
    
    
}
