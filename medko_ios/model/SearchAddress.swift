//
//  SearchAddress.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class SearchAddress : Object, Mappable {
    
    dynamic var id = 0
    dynamic var address = ""
    dynamic var complement = ""
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.address <- map["address"]
        self.complement <- map["complement"]
        self.latitude <- map["latitude"]
        self.longitude <- map["longitude"]
    }
    
}

extension SearchAddress: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: SearchAddress) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [SearchAddress]) {
        try! realm.write {
            realm.delete(realm.objects(SearchAddress.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [SearchAddress]? {
        return [SearchAddress](realm.objects(SearchAddress.self))
    }
    
    static func getByID(id: AnyObject) -> SearchAddress? {
        return realm.object(ofType: SearchAddress.self, forPrimaryKey: id)
    }
}
