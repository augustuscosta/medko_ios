//
//  HealthPlan.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class HealthPlan : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
    
}

extension HealthPlan: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: HealthPlan) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [HealthPlan]) {
        try! realm.write {
            realm.delete(realm.objects(HealthPlan.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [HealthPlan]? {
        return [HealthPlan](realm.objects(HealthPlan.self).sorted(byProperty: "name"))
    }
    
    static func getByID(id: AnyObject) -> HealthPlan? {
        return realm.object(ofType: HealthPlan.self, forPrimaryKey: id)
    }
}


