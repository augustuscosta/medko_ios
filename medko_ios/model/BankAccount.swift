//
//  BankAccount.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class BankAccount: Object, Mappable {
    
    dynamic var id = 0
    dynamic var bank = ""
    dynamic var fullName = ""
    dynamic var cpfCnpj = ""
    dynamic var account = ""
    dynamic var operation = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.bank <- map["bank"]
        self.fullName <- map["full_name"]
        self.cpfCnpj <- map["cpf_cnpj"]
        self.account <- map["account"]
        self.operation <- map["operation"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension BankAccount: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: BankAccount) {
        try! realm.write {
            realm.delete(realm.objects(BankAccount.self))
            realm.add(object)
        }
    }
    
    
    static func saveAll(objects: [BankAccount]) {
        try! realm.write {
            realm.delete(realm.objects(BankAccount.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [BankAccount]? {
        return [BankAccount](realm.objects(BankAccount.self))
    }
    
    static func getByID(id: AnyObject) -> BankAccount? {
        return realm.object(ofType: BankAccount.self, forPrimaryKey: id)
    }
}
