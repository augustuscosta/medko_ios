//
//  Payment.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Payment : Object, Mappable {
    
    dynamic var id = 0
    dynamic var value = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.value <- map["value"]
    }
}

extension Payment: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Payment) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [Payment]) {
        try! realm.write {
            realm.delete(realm.objects(Payment.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [Payment]? {
        return [Payment](realm.objects(Payment.self))
    }
    
    static func getByID(id: AnyObject) -> Payment? {
        return realm.object(ofType: Payment.self, forPrimaryKey: id)
    }
}
