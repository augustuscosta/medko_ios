//
//  PaymentMethod.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class PaymentMethod : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
}

extension PaymentMethod: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: PaymentMethod) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [PaymentMethod]) {
        try! realm.write {
            realm.delete(realm.objects(PaymentMethod.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [PaymentMethod]? {
        return [PaymentMethod](realm.objects(PaymentMethod.self).sorted(byProperty: "name"))
    }
    
    static func getByID(id: AnyObject) -> PaymentMethod? {
        return realm.object(ofType: PaymentMethod.self, forPrimaryKey: id)
    }
}
