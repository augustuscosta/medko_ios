//
//  Place.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Place: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var address: String = ""
    dynamic var complement: String = ""
    dynamic var latitude = Float()
    dynamic var longitude = Float()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.address <- map["address"]
        self.complement <- map["complement"]
        self.latitude <- map["latitude"]
        self.longitude <- map["longitude"]
    }
    
}

extension Place: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Place) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [Place]) {
        try! realm.write {
            realm.delete(realm.objects(Place.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [Place]? {
        return [Place](realm.objects(Place.self))
    }
    
    static func getByID(id: AnyObject) -> Place? {
        return realm.object(ofType: Place.self, forPrimaryKey: id)
    }
}

