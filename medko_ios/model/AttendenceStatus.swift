//
//  AttendenceStatus.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class AttendenceStatus : Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
    }
}

extension AttendenceStatus: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: AttendenceStatus) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [AttendenceStatus]) {
        try! realm.write {
            realm.delete(realm.objects(AttendenceStatus.self))
            realm.add(objects)
        }
    }
    
    static func getAll() -> [AttendenceStatus]? {
        return [AttendenceStatus](realm.objects(AttendenceStatus.self))
    }
    
    static func getByID(id: AnyObject) -> AttendenceStatus? {
        return realm.object(ofType: AttendenceStatus.self, forPrimaryKey: id)
    }
}

