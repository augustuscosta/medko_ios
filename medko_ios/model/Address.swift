//
//  Address.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class Address: Mappable {
    
    dynamic var logradouro = ""
    dynamic var complemento = ""
    dynamic var bairro = ""
    dynamic var localidade = ""
    dynamic var uf = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.logradouro <- map["logradouro"]
        self.complemento <- map["complemento"]
        self.bairro <- map["bairro"]
        self.localidade <- map["localidade"]
        self.uf <- map["uf"]
    }
    
    
    static func getAddressByCep(cep: String) -> String {
        var address = ""
        Alamofire.request("https://viacep.com.br/ws/"+cep+"/json/").responseObject { (response: DataResponse<Address>) in
            if response.result.isSuccess {
                if let obj = response.result.value {
                    address = "\(obj.logradouro), \(obj.complemento) - \(obj.bairro), \(obj.localidade) - \(obj.uf)"
                    print(address)
                }
            }
        }
        return address
    }
}
