//
//  ServiceWindows.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class ServiceWindows: Object, Mappable {
    
    dynamic var id = 0
    dynamic var value = 0.0
    dynamic var media = 0
    dynamic var rating = false
    dynamic var takeTime = ""
    dynamic var place: Place? = Place()
    dynamic var doctor: User? = User()
    dynamic var examType: ExamType? = ExamType()
    dynamic var speciality: Speciality? = Speciality()
    var healthPlan = List<HealthPlan>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.value <- map["value"]
        self.media <- map["media"]
        self.rating <- map["rating"]
        self.takeTime <- map["take_time"]
        self.place <- map["place"]
        self.doctor <- map["user"]
        self.examType <- map["exam_type"]
        self.speciality <- map["speciality"]
        
        var healtPlanArray = [HealthPlan]()
        healtPlanArray <- map["health_plan"]
        
        for item in healtPlanArray {
            self.healthPlan.append(item)
        }
    }
}

extension ServiceWindows: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: ServiceWindows) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [ServiceWindows]) {
        try! realm.write {
            realm.delete(realm.objects(ServiceWindows.self))
            realm.add(objects, update: true)
        }
    }
    
    static func getAll() -> [ServiceWindows]? {
        return [ServiceWindows](realm.objects(ServiceWindows.self))
    }
    
    static func getByID(id: AnyObject) -> ServiceWindows? {
        return realm.object(ofType: ServiceWindows.self, forPrimaryKey: id)
    }
}
