//
//  Attendence.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Attendence: Object, Mappable {
    
    dynamic var id = 0
    dynamic var date = ""
    dynamic var value = 0.0
    dynamic var place: Place? = Place()
    dynamic var searchAddress: SearchAddress? = SearchAddress()
    dynamic var paymentMethod: PaymentMethod? = PaymentMethod()
    dynamic var attendenceType: AttendenceType? = AttendenceType()
    dynamic var attendenceStatus: AttendenceStatus? = AttendenceStatus()
    dynamic var payment: Payment? = Payment()
    dynamic var healthPlan: HealthPlan? = HealthPlan()
    dynamic var classification: Classification? = Classification()
    dynamic var speciality: Speciality? = Speciality()
    dynamic var examType: ExamType? = ExamType()
    dynamic var patient: User? = User()
    dynamic var doctor: User? = User()
    var takeTimes = List<TakeTime>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.date <- map["date"]
        self.value <- map["value"]
        self.place <- map["place"]
        self.searchAddress <- map["search_address"]
        self.paymentMethod <- map["payment_method"]
        self.attendenceType <- map["attendence_type"]
        self.attendenceStatus <- map["attendence_status"]
        self.payment <- map["payment"]
        self.healthPlan <- map["health_plan"]
        self.classification <- map["classification"]
        self.patient <- map["patient"]
        self.doctor <- map["doctor"]
        self.speciality <- map["speciality"]
        self.examType <- map["exam_type"]
        
        
        var takeTimesArray: [TakeTime] = []
        takeTimesArray <- map["take_time"]
        
        self.takeTimes.append(contentsOf: takeTimesArray)
        
    }
}

extension Attendence: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Attendence) {
        try! realm.write {
            realm.add(object, update: true)
        }
    }
    
    static func saveAll(objects: [Attendence]) {
        try! realm.write {
            realm.delete(realm.objects(Attendence.self))
            realm.add(objects, update: true)
        }
    }
    
    
    static func getAll() -> [Attendence]? {
        return [Attendence](realm.objects(Attendence.self))
    }
    
    static func getByID(id: AnyObject) -> Attendence? {
        return realm.object(ofType: Attendence.self, forPrimaryKey: id)
    }
    
}

