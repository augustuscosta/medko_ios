//
//  Speciality.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


class Speciality: Object, Mappable {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var homeCare = false
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.homeCare <- map["home_care"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

extension Speciality: RealmDAO {
    
    static let realm = try! Realm()
    
    static func save(object: Speciality) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func saveAll(objects: [Speciality]) {
        try! realm.write {
            realm.delete(realm.objects(Speciality.self))
            realm.add(objects, update: true)
        }
    }
    
    static func getAll() -> [Speciality]? {
        return [Speciality](realm.objects(Speciality.self).sorted(byProperty: "name"))
    }
    
    static func getByID(id: AnyObject) -> Speciality? {
        return realm.object(ofType: Speciality.self, forPrimaryKey: id)
    }
    
    static func getByName(name: String) -> Speciality? {
        return realm.objects(Speciality.self).filter("name == '\(name)'").first
    }
}


