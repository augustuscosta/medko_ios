//
//  Point.swift
//  medko_ios
//
//  Created by Thialyson Martins on 22/11/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import Foundation

class Point {
    
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    
}
