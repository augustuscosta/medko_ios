//
//  SegueFromRight.swift
//  medko_ios
//
//  Created by Thialyson Martins on 19/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import Foundation
import UIKit

class SegueFromRight: UIStoryboardSegue {
    
    override func perform() {
        
        let src = self.source as UIViewController
        let dst = self.destination as UIViewController
        
        src.view.superview?.insertSubview(dst.view, belowSubview: src.view)
        src.view.transform = CGAffineTransform(translationX: 0, y: 0)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            src.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)
        }, completion: { finished in
            src.dismiss(animated: false, completion: nil)
        })
    }
}
