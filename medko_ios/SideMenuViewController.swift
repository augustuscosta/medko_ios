//
//  SideMenuViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 19/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import AlamofireImage

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var buttonAttendence: UIButton!
    @IBOutlet weak var buttonHistoric: UIButton!
    @IBOutlet weak var buttonLogOut: UIButton!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var nameCurrentUser: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    var accountTypeId = Int()
    var curretUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingBar.settings.fillMode = .full
        if let user = User.getByName(name: Request.nameCurrentUser) {
            accountTypeId = UserDefaults.standard.integer(forKey: "accountTypeId")
            curretUser = user
            setAvatar()
            nameCurrentUser.text = Request.nameCurrentUser
            setRatingBar()
        }
    }
    
    func setRatingBar(){
        if curretUser.rating {
            ratingBar.isHidden = false
            ratingBar.rating = Double(curretUser.media)
        }
    }
    
    func setAvatar(){
        Alamofire.request(Util.root + Request.avatar).responseImage { response in
            switch response.result{
            case .success:
                let size = CGSize(width: 50.0, height: 50.0)
                if let image = response.result.value?.af_imageAspectScaled(toFill: size).af_imageRoundedIntoCircle() {
                    self.avatar.image = image
                }
            default: break
            }
        }
    }
    
    
    @IBAction func buttonAttendenceClick(_ sender: Any) {
        changeButton(button: buttonAttendence)
        if accountTypeId == 1 {
            Util.showView(name: "NewAttendenceViewController", context: self)
        } else if accountTypeId == 3 {
            Util.showView(name: "MapDoctorViewController", context: self)
        }
    }
    
    @IBAction func buttonHistoricClick(_ sender: Any) {
        changeButton(button: buttonHistoric)
    }
    
    @IBAction func buttonLogOutClick(_ sender: Any) {
        changeButton(button: buttonLogOut)
        Request.logout(context: self) { (result, error) in
            if error == nil {
                Session.clearAllTables()
                Util.showView(name: "UITabBarController", context: self)
            }
        }
    }
    
    func changeButton(button: UIButton){
        cleanButtons()
        selectButton(button: button)
    }
    
    func selectButton(button: UIButton) {
        button.backgroundColor = #colorLiteral(red: 0.4836579561, green: 0.3624212742, blue: 0.5225611329, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: UIControlState.normal)
    }
    
    func cleanButtons() {
        clearButton(button: buttonAttendence)
        clearButton(button: buttonHistoric)
        clearButton(button: buttonLogOut)
    }
    
    func clearButton(button: UIButton){
        button.backgroundColor = #colorLiteral(red: 0.9451727271, green: 0.9494821429, blue: 0.9006384015, alpha: 1)
        button.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControlState.normal)
    }
    
}
