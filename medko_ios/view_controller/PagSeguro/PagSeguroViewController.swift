//
//  PagSeguroViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 16/02/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit

class PagSeguroViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    static var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PagSeguroViewController.url.contains("/attendences/"){
            webView.loadRequest(URLRequest(url: URL(string: PagSeguroViewController.url)!))
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
