//
//  RegisterPatientViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 23/12/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import Fusuma

class RegisterPatientViewController: UIViewController, FusumaDelegate {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var namePatient: UITextField!
    @IBOutlet weak var emailPatient: UITextField!
    @IBOutlet weak var phonePatient: UITextField!
    @IBOutlet weak var passwordPatient: UITextField!
    @IBOutlet weak var confirPasswordPatient: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func validateFields(_ sender: Any) {
        var list: [Bool] = []
        list.append(Validate.check(field: namePatient.text!, min: 2, max: 100, name: "Nome", context: self))
        list.append(Validate.check(field: emailPatient.text!, min: 1, max: 100, name: "Email", context: self))
        list.append(Validate.check(field: phonePatient.text!, min: 10, max: 11, name: "Telefone", context: self))
        list.append(Validate.check(field: passwordPatient.text!, min: 6, max: 50, name: "Senha", context: self))
        list.append(Validate.checkConfirmPass(pass: passwordPatient.text!, confirmPass: confirPasswordPatient.text!, name: "Confirmar Password", context: self))
        
        if Validate.checkIfAllAreTrue(list: list){
            Request.register(parameters: returnUser(),
                             idsSpecialities: nil,
                             idsExams: nil,
                             avatar: imageUser.image != nil ? imageUser.image! : nil,
                             crm: nil,
                             proofOfAddress: nil,
                             context: self)
        }
    }
    
    func returnUser() -> [String : String] {
        return [
            "user[name]" : namePatient.text!,
            "user[email]" : emailPatient.text!,
            "user[telephone]" : phonePatient.text!,
            "user[password]" : passwordPatient.text!,
            "user[confirm_password]": confirPasswordPatient.text!,
            "user[account_type_id]" : "1",]
    }
    
    @IBAction func imageTapped(_ sender: Any) {
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false
        fusumaCropImage = true
        
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) { }
    func fusumaDismissedWithImage(_ image: UIImage) { }
    func fusumaClosed() { }
    func fusumaImageSelected(_ image: UIImage) {
        imageUser.image = image
    }
    
    func fusumaCameraRollUnauthorized() {
        self.dismiss(animated: true, completion: nil)
        Util.showAlert(alert: Util.alertNotAllowed(), context: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    
}
