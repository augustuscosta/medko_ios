//
//  MapPatientViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 01/02/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireObjectMapper

class MapPatientViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var mapPatient: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var viewTop: UIView!
    
    var searchParameterss = [String: String]()
    
    var attendenceTypeId = String()
    var healthPlanId = String()
    var paymentMethodId = String()
    var examTypeId = String()
    var specialityId = String()
    var serviceTypeId = String()
    
    var address = ""
    var manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Util.getShadow(view: searchBar)
        Util.getShadow(view: locationButton)
        Util.getShadow(view: nextButton)
        Util.getShadow(view: viewTop)
        
        
        getIdsFromPreferences()
        
        searchBar.delegate = self
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        //        -3.727922, -38.523945 rua pedro borges - zoom 0.02
        let coordinate = CLLocationCoordinate2DMake(-3.727922, -38.523945)
        let span = MKCoordinateSpanMake(0.02, 0.02)
        let region = MKCoordinateRegionMake(coordinate, span)
        
        self.mapPatient.setRegion(region, animated: true)
    }
    
    @IBAction func locationButtonClick(_ sender: Any) {
        mapPatient.setCenter(mapPatient.userLocation.coordinate, animated: true)
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        let center = mapPatient.camera.centerCoordinate
        searchByLocation(center: center)
    }
    
    func searchByLocation(center: CLLocationCoordinate2D) {
        CLGeocoder().geocodeAddressString("\(center.latitude) \(center.longitude)") { (place, error) in
            self.setAddress(place: place!)
            self.setParams(center: center)
        }
    }
    
    func setParams(center: CLLocationCoordinate2D){
        let searchParameters = returnSearchParameters(center: center)
        searchAttendence(searchParameters: searchParameters, center: center)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != nil {
            searchByAddress(address: searchBar.text!)
        }
    }
    
    func searchByAddress(address: String) {
        CLGeocoder().geocodeAddressString(address) { (place, error) in
            if error == nil {
                let span = MKCoordinateSpanMake(0.005, 0.005)
                let region = MKCoordinateRegionMake(place!.first!.location!.coordinate, span)
                self.mapPatient.setRegion(region, animated: true)
                
                self.setAddress(place: place!)
            }
        }
    }
    
    func setAddress(place: [CLPlacemark]){
        if let local = place.first {
            let street = local.thoroughfare != nil ? local.thoroughfare! : ""
            let number = local.subThoroughfare != nil ? local.subThoroughfare! : ""
            let subLocality = local.subLocality != nil ? local.subLocality! : ""
            let city = local.locality != nil ? local.locality! : ""
            let state = local.administrativeArea != nil ? local.administrativeArea! : ""
            
            self.address = street+", "+number+" - "+subLocality+" - "+city+" - "+state
        }
    }
    
    func returnSearchParameters(center: CLLocationCoordinate2D) -> [String : String] {
        return [
            "latitude" : String(center.latitude),
            "longitude" : String(center.longitude),
            "attendence_type" : attendenceTypeId,
            "health_plan": healthPlanId,
            "payment" : paymentMethodId,
            "speciality" : specialityId,
            "exam" : examTypeId,
            "address" : address,
        ]
    }
    
    func searchAttendence(searchParameters: [String : String], center: CLLocationCoordinate2D) {
        if attendenceTypeId == "1" {
            searchAttendenceHomeCare(params: searchParameters)
        } else {
            callServiceWindowsList(center: center)
        }
    }
    
    func searchAttendenceHomeCare(params: [String : String]){
        Request.getAttendeceHomeCare(searchParameters: params, handler: { (attendence, error) in
            
            if error == nil {
                AttendenceListViewController.attendenceId = attendence!.id
                Util.showView(name: "AttendenceViewController", context: self)
            } else {
                Util.showAlert(alert: Util.alertNotFound(context: self), context: self)
            }
        })
    }
    
    func callServiceWindowsList(center: CLLocationCoordinate2D) {
        UserDefaults.standard.set(center.latitude, forKey: "latitude")
        UserDefaults.standard.set(center.longitude, forKey: "longitude")
        Util.showView(name: "ServiceWindowsViewController", context: self)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .denied {
            Util.showAlert(alert: Util.alertNotAllowed(), context: self)
        }
    }
    
    func getIdsFromPreferences(){
        attendenceTypeId = getDefaults(key: "attendenceTypeId")
        healthPlanId = getDefaults(key:"healthPlanId")
        paymentMethodId = getDefaults(key:"paymentMethodId")
        examTypeId = getDefaults(key: "examTypeId")
        specialityId = getDefaults(key: "specialityId")
        serviceTypeId = getDefaults(key: "serviceTypeId")
    }
    
    func getDefaults(key: String) -> String {
        if let result = UserDefaults.standard.string(forKey: key) {
            return result
        }
        return ""
    }
}
