//
//  MapDoctorViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 01/02/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import MapKit

class MapDoctorViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var switchLocation: UISwitch!
    @IBOutlet weak var mapDoctor: MKMapView!
    
    var manager = CLLocationManager()
    var timer : Timer?
    var latitude = 0.0
    var longitude = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        manager.allowsBackgroundLocationUpdates = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        latitude = locations.last!.coordinate.latitude
        longitude = locations.last!.coordinate.longitude
        
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let span = MKCoordinateSpanMake(0.02, 0.02)
        let region = MKCoordinateRegionMake(coordinate, span)
        
        mapDoctor.setRegion(region, animated: true)
    }
    
    @IBAction func changeState(_ sender: UISwitch) {
        if sender.isOn {
            startUpdateLocation()
        } else {
            stopUpdateLocation()
        }
    }
    
    func startUpdateLocation(){
        if latitude != 0.0 {
            timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { _ in
                let params = ["latitude": "\(self.latitude)", "longitude" : "\(self.longitude)"]
                Request.changeLocation(params: params, context: self)
            }
        }
    }
    
    func stopUpdateLocation(){
        if timer != nil {
            print("parou!!")
            timer?.invalidate()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            Util.showAlert(alert: Util.alertNotAllowed(), context: self)
        }
    }
    
}
