//
//  LoginPatientViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 24/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import FacebookCore
import FacebookLogin

class LoginPatientViewController: UIViewController {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var emailPatientLogin: UITextField!
    @IBOutlet weak var passwordPatientLogin: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        emailPatientLogin.layer.borderColor = UIColor(red: 187, green: 187, blue: 187, alpha: 1).cgColor
        
        Util.getShadow(view: viewTop)
        
        if let result = Session.getAll() {
            if result.count > 0 {
                self.present(Util.getView(storyboard: "Main", viewController: "AttendenceListViewController"), animated: true, completion: nil)
//                Util.showView(name: "AttendenceListViewController", context: self)
            }
        }
    }
    
    @IBAction func validateFields(_ sender: Any) {
        
        var list: [Bool] = []
        list.append(Validate.check(field: self.emailPatientLogin.text!, min: 1, max: 100, name: "Email", context: self))
        list.append(Validate.check(field: self.passwordPatientLogin.text!, min: 6, max: 50, name: "Senha", context: self))
        
        if Validate.checkIfAllAreTrue(list: list){
            
            let session: [String: String] = ["email": self.emailPatientLogin.text!,"password": self.passwordPatientLogin.text!]
            let url = "\(Util.rootUrl)users/sign_in_mobile"
            
            Request.login(url: url, params: session, paramsFb: [:], context: self)
        }
    }
    
    @IBAction func facebookButtonClick(_ sender: Any) {
        LoginManager().logIn([.publicProfile, .email], viewController: self) { result in
            switch result {
                
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                self.setAttributesFb(token: String(describing: accessToken.authenticationToken))
            }
        }
    }
    
    func setAttributesFb(token: String){
        let params = ["fields" : "email, name, id"]
        
        GraphRequest(graphPath: "me", parameters: params).start { (response, result) in
            switch result {
                
            case .failed(let error):
                print("error in graph request:", error)
                break
                
            case .success(let graphResponse):
                if let responseDict = graphResponse.dictionaryValue {
                    
                    let url = "\(Util.rootUrl)/users/sign_in_mobile_facebook"
                    let userId = responseDict["id"] as! String
                    let avatarUrl = "http://graph.facebook.com/\(userId)/picture?type=large"
                    let userFacebook = ["user" : ["name" : responseDict["name"]! as! String, "email" : responseDict["email"]! as! String, "account_type_id" : 1, "facebook_account": true, "avatar" : avatarUrl, "facebook_token" : token]]
                    
                    Request.login(url: url, params: [:], paramsFb: userFacebook, context: self)
                }
            }
        }
    }
    
}
