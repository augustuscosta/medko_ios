//
//  LoginDoctorViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 24/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit

class LoginDoctorViewController: UIViewController {

    @IBOutlet weak var emailDoctor: UITextField!
    @IBOutlet weak var passwordDoctor: UITextField!
    @IBOutlet weak var viewTop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.getShadow(view: viewTop)
    }
    
    @IBAction func validateFields(_ sender: Any) {
        
        var list: [Bool] = []
        list.append(Validate.check(field: emailDoctor.text!, min: 1, max: 100, name: "Email", context: self))
        list.append(Validate.check(field: passwordDoctor.text!, min: 6, max: 50, name: "Senha", context: self))
        
        if Validate.checkIfAllAreTrue(list: list) {
            let session = ["email": emailDoctor.text!,"password": passwordDoctor.text!]
            let url = "\(Util.rootUrl)/users/sign_in_mobile"
            Request.login(url: url, params: session, paramsFb: [:], context: self)
        }

    }

}
