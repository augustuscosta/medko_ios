//
//  ServiceWindowsViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 10/02/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Cosmos

class ServiceWindowsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var serviceWindowsTableView: UITableView!
    
    var serviceWindowsList = [ServiceWindows]()
    var serviceTypeId = ""
    var healthPlanName = ""
    var healthPlanId = ""
    var paymentMethodId = ""
    var serviceWindowsId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        serviceTypeId = getDefaults(key: "serviceTypeId")
        healthPlanName = getDefaults(key: "healthPlanName")
        healthPlanId = getDefaults(key: "healthPlanId")
        paymentMethodId = getDefaults(key: "paymentMethodId")
        
        let params = [
            "latitude" : getDefaults(key: "latitude"),
            "longitude" : getDefaults(key: "longitude"),
            "attendence_type" : getDefaults(key: "attendenceTypeId"),
            "health_plan": getDefaults(key: "healthPlanId"),
            "payment" : getDefaults(key: "paymentMethodId"),
            "speciality" : getDefaults(key: "specialityId"),
            "exam" : getDefaults(key: "examTypeId"),
            ]
        
        print(params)
        
        Request.getServiceWindowsList(searchParameters: params) { (serviceWindowsList, error) in
            if error == nil {
                if let result = serviceWindowsList {
                    if result.count > 0 {
                        self.serviceWindowsList.append(contentsOf: result)
                        self.serviceWindowsTableView.delegate = self
                        self.serviceWindowsTableView.dataSource = self
                        self.serviceWindowsTableView.reloadData()
                    } else {
                        Util.showAlert(alert: Util.alertNotFound(context: self), context: self)
                    }
                }
            }
        }
    }
    
//    func returnSearchParameters() -> [String : String] {
//        return [
//            "latitude" : getDefaults(key: "latitude"),
//            "longitude" : getDefaults(key: "longitude"),
//            "attendence_type" : getDefaults(key: "attendenceTypeId"),
//            "health_plan": getDefaults(key: "healthPlanId"),
//            "payment_method" : getDefaults(key: "paymentMethodId"),
//            "speciality" : getDefaults(key: "specialityId"),
//            "examType" : getDefaults(key: "examTypeId"),
//        ]
//    }
    
    func getDefaults(key: String) -> String {
        return UserDefaults.standard.string(forKey: key) != nil ? UserDefaults.standard.string(forKey: key)! : ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.serviceWindowsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceWindowsCell", for: indexPath) as! ServiceWindowsTableViewCell
        let serviceWindows = serviceWindowsList[indexPath.row]
        
        
        Util.getShadow(view: cell)
        
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        cell.layer.borderWidth = 2
        
        
        cell.name.text = serviceWindows.doctor!.name
        cell.place.text = serviceWindows.place!.name
        cell.address.text = serviceWindows.place!.address
        cell.date.text = serviceWindows.takeTime
        
        setSpeciality(cell: cell, serviceWindow: serviceWindows)
        setRatingBar(cell: cell, serviceWindow: serviceWindows)
        setPrice(cell: cell, serviceWindow: serviceWindows)
        
        return cell
    }
    
    func setSpeciality(cell: UITableViewCell, serviceWindow: ServiceWindows){
        let celula = cell as! ServiceWindowsTableViewCell
        if serviceTypeId != "" && serviceTypeId == "1" {
            celula.speciality.isHidden = false
            celula.speciality.text = serviceWindow.speciality?.name != nil ? serviceWindow.speciality!.name : ""
        } else if serviceTypeId != "" && serviceTypeId == "2" {
            celula.examType.isHidden = false
            celula.examType.text = serviceWindow.examType?.name != nil ? serviceWindow.examType!.name : ""
        }
    }

    func setRatingBar(cell: UITableViewCell, serviceWindow: ServiceWindows){
        let celula = cell as! ServiceWindowsTableViewCell
        if serviceWindow.doctor!.rating && serviceWindow.doctor!.media > 0 {
            celula.ratingBar.isHidden = false
            celula.ratingBar.settings.fillMode = .full
            celula.ratingBar.rating = Double(serviceWindow.doctor!.media)
        }
    }
    
    func setPrice(cell: UITableViewCell, serviceWindow: ServiceWindows){
        let celula = cell as! ServiceWindowsTableViewCell
        if !serviceWindow.healthPlan.isEmpty && healthPlanName != "" {
            celula.price.text = healthPlanName
        } else {
            celula.price.text = "R$ "+String.init(format: "%.2f", serviceWindow.value).replacingOccurrences(of: ".", with: ",")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        serviceWindowsId = serviceWindowsList[indexPath.row].id
        let params = returnSelectedItens()
        print(params)
        createNewAttendence(params: params)
    }
    
    func createNewAttendence(params: [String: Any]){
        Request.createAttendence(params: params) { (attendence, error) in
            if error == nil {
                AttendenceListViewController.attendenceId = attendence!.id
                Util.showView(name: "AttendenceViewController", context: self)
            } else {
                print("debug")
            }
        }
    }
    
    func returnSelectedItens() -> [String: Any]{
        return [
            "id" : "\(serviceWindowsId)",
            "health" : healthPlanId != "" ? healthPlanId : "",
            "payment" : paymentMethodId != "" ? paymentMethodId : "",
        ]
    }
    
}
