//
//  ServiceWindowsTableViewCell.swift
//  medko_ios
//
//  Created by Thialyson Martins on 10/02/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Cosmos

class ServiceWindowsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageUser: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var speciality: UILabel!
    @IBOutlet weak var examType: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var address: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
