//
//  RegisterDoctorViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 23/12/16.
//  Copyright © 2016 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import Fusuma

class RegisterDoctorViewController: UIViewController, FusumaDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameDoctor: UITextField!
    @IBOutlet weak var crmDoctor: UITextField!
    @IBOutlet weak var phoneDoctor: UITextField!
    @IBOutlet weak var switchAccept: UISwitch!
    @IBOutlet weak var valueHomeCare: UITextField!
    @IBOutlet weak var emailDoctor: UITextField!
    @IBOutlet weak var passwordDoctor: UITextField!
    @IBOutlet weak var confirmPasswordDoctor: UITextField!
    @IBOutlet weak var zipCode: UITextField!
    @IBOutlet weak var addressDoctor: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var cpfCnpj: UITextField!
    @IBOutlet weak var bank: UITextField!
    @IBOutlet weak var agency: UITextField!
    @IBOutlet weak var account: UITextField!
    @IBOutlet weak var operation: UITextField!
    @IBOutlet weak var proofOfAddressTextField: UITextField!
    @IBOutlet weak var crmTextField: UITextField!
    
    var keyImage = 0
    var crmImage = UIImageView()
    var proofOfAddressImage = UIImageView()
    
    var addressText = ""
    
    static var titleList = ""
    static var namesArray = [String]()
    static var key = Int()
    static var indexArray = [Int]()
    
    static var specialityNameList = Request.specialityNameList
    static var examTypeNameList = Request.examTypeNameList
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Request.requestSpecialities(context: self)
        Request.requestExamTypes(context: self)
        
        zipCode.delegate = self
        addressDoctor.delegate = self
    }
    
    @IBAction func specialityButtonClick(_ sender: UIButton) {
        if !RegisterDoctorViewController.specialityNameList.isEmpty {
            
            changeParams(title: "Especialidades", key: 1, arrayName: RegisterDoctorViewController.specialityNameList)
            Util.showView(name: "TableViewController", context: self)
        }
    }
    
    @IBAction func exameTypeButtonClick(_ sender: UIButton) {
        if !RegisterDoctorViewController.examTypeNameList.isEmpty {
            
            changeParams(title: "Exames", key: 2, arrayName: RegisterDoctorViewController.examTypeNameList)
            Util.showView(name: "TableViewController", context: self)
        }
    }
    
    func changeParams(title: String, key: Int, arrayName: [String]) {
        RegisterDoctorViewController.titleList = title
        RegisterDoctorViewController.key = key
        RegisterDoctorViewController.namesArray.removeAll()
        RegisterDoctorViewController.namesArray = arrayName
    }
    
    @IBAction func validateFields(_ sender: Any) {
        
        
        var list: [Bool] = []
        list.append(Validate.check(field: nameDoctor.text!, min: 1, max: 100, name: "Nome", context: self))
        list.append(Validate.check(field: crmDoctor.text!, min: 1, max: 50, name: "CRM", context: self))
        list.append(Validate.check(field: phoneDoctor.text!, min: 10, max: 11, name: "Telefone", context: self))
        list.append(Validate.check(field: emailDoctor.text!, min: 1, max: 100, name: "Email", context: self))
        list.append(Validate.check(field: passwordDoctor.text!, min: 6, max: 50, name: "Senha", context: self))
        list.append(Validate.checkConfirmPass(pass: passwordDoctor.text!, confirmPass: confirmPasswordDoctor.text!, name: "Confirmar Password", context: self))
        list.append(Validate.check(field: zipCode.text!, min: 8, max: 8, name: "CEP", context: self))
        list.append(Validate.check(field: addressDoctor.text!, min: 1, max: 100, name: "Endereço", context: self))
        list.append(Validate.check(field: fullName.text!, min: 1, max: 100, name: "Nome Completo", context: self))
        list.append(Validate.check(field: cpfCnpj.text!, min: 11, max: 14, name: "CPF/CNPJ", context: self))
        list.append(Validate.check(field: bank.text!, min: 1, max: 50, name: "Banco", context: self))
        list.append(Validate.check(field: agency.text!, min: 1, max: 50, name: "Agência", context: self))
        list.append(Validate.check(field: account.text!, min: 1, max: 50, name: "Conta", context: self))
        list.append(Validate.check(field: operation.text!, min: 1, max: 50, name: "Operação", context: self))
        list.append(Validate.checkImage(image: crmImage.image, name: "Foto CRM", context: self))
        list.append(Validate.checkImage(image: proofOfAddressImage.image, name: "Foto Comprovante de Residência", context: self))
        
        
        if Validate.checkIfAllAreTrue(list: list){
            
            Request.register(parameters: returnUser(),
                             idsSpecialities: !TableViewController.selectedSpecialities.isEmpty ? TableViewController.selectedSpecialities : nil,
                             idsExams: !TableViewController.selectedExamTypes.isEmpty ? TableViewController.selectedExamTypes : nil,
                             avatar: imageUser.image != nil ? imageUser.image! : nil,
                             crm: crmImage.image != nil ? crmImage.image : nil,
                             proofOfAddress: proofOfAddressImage.image != nil ? proofOfAddressImage.image : nil,
                             context: self)
        }
        
    }
    
    func returnUser() -> [String : String] {
        let user = [
            "user[name]" : nameDoctor.text!,
            "user[crm]" : crmTextField.text!,
            "user[email]" : emailDoctor.text!,
            "user[value_home_care]" : valueHomeCare.text != nil ? valueHomeCare.text! : "",
            "user[telephone]" : phoneDoctor.text!,
            "user[password]" : passwordDoctor.text!,
            "user[password_confirmation]": confirmPasswordDoctor.text!,
            "user[zip_code]": zipCode.text!,
            "user[address]": addressDoctor.text!,
            "user[account_type_id]" : "3",
            "user[active]" : "true",
            "user[rating]" : String(switchAccept.isOn ? true : false),
            "user[bank_account_attributes][0][full_name]" : fullName.text!,
            "user[bank_account_attributes][0][cpf_cnpj]" : cpfCnpj.text!,
            "user[bank_account_attributes][0][bank]" : bank.text!,
            "user[bank_account_attributes][0][account]" : account.text!,
            "user[bank_account_attributes][0][agency]" : agency.text!,
            "user[bank_account_attributes][0][operation]" : operation.text!,
            ]
        
        return user
    }
    
    @IBAction func imageTapped(_ sender: Any) {
        keyImage = 1
        callFusumaView()
    }
    
    func callFusumaView(){
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false
        fusumaCropImage = true
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL){}
    func fusumaDismissedWithImage(_ image: UIImage){}
    func fusumaClosed(){}
    func fusumaImageSelected(_ image: UIImage) {
        switch keyImage {
        case 1:
            imageUser.image = image
            break
        case 2:
            setImage(image: image, field: crmImage, textField: crmTextField)
            break
        case 3:
            setImage(image: image, field: proofOfAddressImage, textField: proofOfAddressTextField)
            break
        default :
            break
        }
    }
    
    func setImage(image: UIImage, field: UIImageView, textField: UITextField) {
        field.image = image
        textField.font = UIFont(name: "Dosis-Bold", size: 15)
        textField.textColor = #colorLiteral(red: 0.4316270351, green: 0.7640123963, blue: 0.6546236873, alpha: 1)
    }
    
    func fusumaCameraRollUnauthorized() {
       self.dismiss(animated: true, completion: nil)
        Util.showAlert(alert: Util.alertNotAllowed(), context: self)
    }
    
    @IBAction func crmImageClick(_ sender: Any) {
        keyImage = 2
        callFusumaView()
    }
    
    @IBAction func proofOfAddressImageClick(_ sender: Any) {
        keyImage = 3
        callFusumaView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if addressText != "" {
            addressDoctor.text = addressText
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != nil {
            getAddressByCep(cep: textField.text!)
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        textField.resignFirstResponder()
        return false;
    }
    
    func getAddressByCep(cep: String) {
        Alamofire.request("https://viacep.com.br/ws/"+cep+"/json/").responseObject { (response: DataResponse<Address>) in
            if response.result.isSuccess {
                if let obj = response.result.value {
                    self.addressDoctor.text = "\(obj.logradouro), \(obj.complemento) - \(obj.bairro), \(obj.localidade) - \(obj.uf)"
                }
            }
        }
    }
    
    
}
