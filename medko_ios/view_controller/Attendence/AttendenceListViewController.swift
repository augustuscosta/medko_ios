//
//  AttendenceListViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 24/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class AttendenceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewToShadow: UIView!
    @IBOutlet weak var tableViewAttendence: UITableView!
    
    static var attendenceList = [Attendence]()
    static var attendenceId: Int?
    var accountTypeCurrentUser: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.getShadow(view: viewToShadow)
        accountTypeCurrentUser = UserDefaults.standard.integer(forKey: "accountTypeId")
        Request.getAttendeces { (attendeceList, error) in
            if error == nil {
                if let _ =  attendeceList {
                    self.delegateItens()
                }
            } else {
                print("Debug")
            }
        }
    }
    
    func delegateItens(){
        tableViewAttendence.delegate = self
        tableViewAttendence.dataSource = self
        tableViewAttendence.reloadData()
        
        listenRefresh()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AttendenceListViewController.attendenceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendenceCell", for: indexPath) as! AttendenceCell
        Util.getShadow(view: cell)
        
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        cell.layer.borderWidth = 2
        
        let attendence = AttendenceListViewController.attendenceList[indexPath.row]
        if !AttendenceListViewController.attendenceList.isEmpty {
            
            setPersonalAttributtes(cell: cell, attendence: attendence)
            setDate(cell: cell, attendence: attendence)
            setStatusAttendence(cell: cell, attendence: attendence)
            cell.specialityAttendenceList.text = attendence.speciality != nil ? attendence.speciality!.name : ""
            cell.examTypeAttendenceList.text = attendence.examType != nil ? attendence.examType!.name : ""
            cell.addressAttendenceList.text = attendence.place?.name != "" ? attendence.place!.name : "Domicílio"
            
        }
        return cell
    }
    
    func setPersonalAttributtes(cell: UITableViewCell, attendence: Attendence){
        let celula = cell as! AttendenceCell
        if accountTypeCurrentUser == 1 {
            celula.avatarAttendenceList.image = #imageLiteral(resourceName: "ic_doctor")
            celula.nameAttendenceList.text = attendence.doctor != nil ? attendence.doctor!.name : ""
            setRatingBar(cell: cell, attendence: attendence)
        } else {
            celula.avatarAttendenceList.image = #imageLiteral(resourceName: "ic_user")
            celula.nameAttendenceList.text = attendence.patient != nil ? attendence.patient!.name : ""
        }
    }
    
    func setDate(cell: UITableViewCell, attendence: Attendence){
        let celula = cell as! AttendenceCell
        if attendence.date != "" {
            let attDate = Util.getDate(dateStr: attendence.date.replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: ".000Z", with: ""))
            
            celula.dateAttendenceList.text = Util.getDateString(date: attDate!)
        } else {
            celula.dateAttendenceList.text = "Não Confirmado"
        }
    }
    
    func setRatingBar(cell: UITableViewCell, attendence: Attendence){
        let celula = cell as! AttendenceCell
        if (attendence.doctor?.rating)! && (attendence.doctor?.media)! > 0 {
            celula.ratingBar.isHidden = false
            celula.ratingBar.settings.fillMode = .full
            celula.ratingBar.rating = Double((attendence.doctor?.media)!)
        }
    }
    
    func setStatusAttendence(cell: UITableViewCell, attendence: Attendence){
        let celula = cell as! AttendenceCell
        let id = attendence.attendenceStatus!.id
        celula.statusAttendenceList.text = attendence.attendenceStatus!.name
        
        if id == 1 || id == 5 {
            celula.statusAttendenceList.textColor = #colorLiteral(red: 0.4316270351, green: 0.7640123963, blue: 0.6546236873, alpha: 1)
        } else if id == 2 || id == 3 {
            celula.statusAttendenceList.textColor = #colorLiteral(red: 0.5990125537, green: 0.3911141753, blue: 0.6621472836, alpha: 1)
        } else {
            celula.statusAttendenceList.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let att = Attendence.getAll()
        AttendenceListViewController.attendenceId = att?[indexPath.row].id
        Util.showView(name: "AttendenceViewController", context: self)
    }
    
    
    func listenRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableViewAttendence.refreshControl = refreshControl
        } else {
            tableViewAttendence.backgroundView = refreshControl
        }
    }
    
    func refresh(_ refreshControl: UIRefreshControl) {
        viewDidLoad()
        refreshControl.endRefreshing()
    }
    
    
    
}
