//
//  NewAttendenceViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 31/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import DropDown


class NewAttendenceViewController: UIViewController {
    
    @IBOutlet weak var buttonPlace: UIButton!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var buttonHelthPlan: UIButton!
    @IBOutlet weak var buttonPaymentMethod: UIButton!
    @IBOutlet weak var buttonServiceType: UIButton!
    @IBOutlet weak var buttonSpeciality: UIButton!
    @IBOutlet weak var buttonExam: UIButton!
    
    @IBOutlet weak var labelPayment: UILabel!
    @IBOutlet weak var labelHealthPlan: UILabel!
    @IBOutlet weak var labelPaymentMethod: UILabel!
    @IBOutlet weak var labelServiceType: UILabel!
    @IBOutlet weak var labelSpeciality: UILabel!
    @IBOutlet weak var labelExam: UILabel!
    @IBOutlet weak var labelEmergency: UILabel!
    
    var iPlace = Int()
    var iPayment = Int()
    var iHealthPlan = Int()
    var iPaymentMethod = Int()
    var iServiceType = Int()
    var iSpeciality = Int()
    var iExam = Int()
    
    var attendenceTypeId = String()
    var healthPlanId = String()
    var healthPlanName = String()
    var paymentMethodId = String()
    var examTypeId = String()
    var specialityId = String()
    var serviceTypeId = String()
    
    var examTypeName = String()
    var specialityName = String()
    
    var placeNameList = [String]()
    var paymentNameList = ["Particular", "Plano de Saúde"]
    var healthPlanNameList = [String]()
    var paymentMethodNameList = [String]()
    var serviceTypeNameList = ["Consulta", "Exame"]
    var specialityNameList = [String]()
    var examTypeNameList = [String]()
    
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Request.requestAttendenceType(context: self)
        Request.requestPaymentMethod(context: self)
        Request.requestHelathPlan(context: self)
        Request.requestSpecialities(context: self)
        Request.requestExamTypes(context: self)
    }
    
    @IBAction func buttonPlaceClick(_ sender: Any) {
        self.placeNameList = Request.attendeceTypeNameList
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonPlace, options: placeNameList, key: "Place")
        dropDown.show()
    }
    
    @IBAction func buttonPaymentClick(_ sender: Any) {
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonPayment, options: paymentNameList, key: "Payment")
        dropDown.show()
    }
    
    @IBAction func buttonHealthPlanClick(_ sender: Any) {
        self.healthPlanNameList = Request.healthPlanNameList
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonHelthPlan, options: healthPlanNameList, key: "HealthPlan")
        dropDown.show()
    }
    
    @IBAction func buttonPaymentMethodClick(_ sender: Any) {
        self.paymentMethodNameList = Request.paymentMethodNameList
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonPaymentMethod, options: paymentMethodNameList, key: "PaymentMethod")
        dropDown.show()
    }
    
    @IBAction func buttonServiceTypeClick(_ sender: Any) {
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonServiceType, options: serviceTypeNameList, key: "ServiceType")
        dropDown.show()
    }
    
    @IBAction func buttonSpecialityClick(_ sender: Any) {
        self.specialityNameList = iPlace == 0 ? Request.specialityNameListHomecare : Request.specialityNameList
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonSpeciality, options: specialityNameList, key: "Speciality")
        dropDown.show()
    }
    
    @IBAction func buttonExamClick(_ sender: Any) {
        self.examTypeNameList = iPlace == 0 ? Request.examTypeNameListHomeCare : Request.examTypeNameList
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonExam, options: examTypeNameList, key: "ExamType")
        dropDown.show()
    }
    
    func getDropDown(dropDown: DropDown, anchor: UIButton, options: [String], key: String) -> DropDown {
        dropDown.anchorView = anchor
        dropDown.dataSource = options
        dropDown.selectionAction = { (index: Int, item: String) in
            
            anchor.setTitle(item, for: UIControlState.normal)
            
            switch key {
            case "Place":
                self.iPlace = index
                self.showSpinnersByPlace()
                break
            case "Payment":
                self.iPayment = index
                self.showSpinnersByPayment()
                break
            case "HealthPlan":
                self.iHealthPlan = index
                self.healthPlanName = item
                break
            case "PaymentMethod":
                self.iPaymentMethod = index
                break
            case "ServiceType":
                self.iServiceType = index
                self.showSpinnersByServiceType()
                break
            case "Speciality":
                self.iSpeciality = index
                self.specialityName = item
                break
            case "ExamType":
                self.iExam = index
                self.examTypeName = item
                break
            default: break
                
            }
        }
        return dropDown
    }
    
    func showSpinnersByPlace(){
        
        if  iPlace == 1 {
            hideAll()
            showPayment()
        } else if iPlace == 0 {
            hideAll()
            showServiceType()
            labelEmergency.isHidden = false
        } else {
            hideAll()
        }
    }
    
    func hideAll(){
        buttonPayment.isHidden = true
        buttonHelthPlan.isHidden = true
        buttonPaymentMethod.isHidden = true
        buttonSpeciality.isHidden = true
        buttonExam.isHidden = true
        
        labelPayment.isHidden = true
        labelHealthPlan.isHidden = true
        labelPaymentMethod.isHidden = true
        labelSpeciality.isHidden = true
        labelExam.isHidden = true
        labelEmergency.isHidden = true
    }
    
    func showServiceType(){
        buttonServiceType.isHidden = false
        labelServiceType.isHidden = false
    }
    
    func showPayment(){
        buttonPayment.isHidden = false
        labelPayment.isHidden = false
    }
    
    func showSpinnersByPayment(){
        if iPayment == 1 {
            showOnlyHealthPlan()
        } else {
            showOnlyPaymentMethod()
        }
    }
    
    func showOnlyHealthPlan() {
        buttonPaymentMethod.isHidden = true
        labelPaymentMethod.isHidden = true
        buttonHelthPlan.isHidden = false
        labelHealthPlan.isHidden = false
        showServiceType()
    }
    
    func showOnlyPaymentMethod() {
        buttonHelthPlan.isHidden = true
        labelHealthPlan.isHidden = true
        buttonPaymentMethod.isHidden = false
        labelPaymentMethod.isHidden = false
        showServiceType()
    }
    
    func showSpinnersByServiceType(){
        if iServiceType == 1 {
            showOnlyExamType()
        } else {
            showOnlySpeciality()
        }
    }
    
    func showOnlyExamType(){
        buttonSpeciality.isHidden = true
        labelSpeciality.isHidden = true
        buttonExam.isHidden = false
        labelExam.isHidden = false
    }
    func showOnlySpeciality(){
        buttonExam.isHidden = true
        labelExam.isHidden = true
        buttonSpeciality.isHidden = false
        labelSpeciality.isHidden = false
    }
    
    
    @IBAction func buttonNextClick(_ sender: Any) {
        if !labelSpeciality.isHidden || !labelExam.isHidden {
            setIdSelectedItems()
        }
    }
    
    func setIdSelectedItems(){
        attendenceTypeId = String(Int(iPlace)+1)
        healthPlanId = getIdHealthPlan(label: labelHealthPlan)!
        paymentMethodId = getIdHealthPlan(label: labelPaymentMethod)!
        examTypeId = getIdExamType(label: labelExam)!
        specialityId = getIdSpeciality(label: labelSpeciality)!
        serviceTypeId = String(Int(iServiceType)+1)
        saveOnPreferences()
    }
    
    func getIdHealthPlan(label: UILabel) -> String? {
        if !label.isHidden {
            return String(Request.healthPlanList[iHealthPlan].id)
        }
        return ""
    }
    
    func getIdPaymentMethod(label: UILabel) -> String? {
        if !label.isHidden {
            return String(Request.paymentMethodList[iPaymentMethod].id)
        }
        return ""
    }
    
    func getIdSpeciality(label: UILabel) -> String? {
        if !label.isHidden {
            return String(describing: Speciality.getByName(name: specialityName)!.id)
        }
        return ""
    }
    
    func getIdExamType(label: UILabel) -> String? {
        if !label.isHidden {
            return String(describing: ExamType.getByName(name: examTypeName)!.id)
        }
        return ""
    }
    
    func saveOnPreferences(){
        UserDefaults.standard.set(attendenceTypeId, forKey: "attendenceTypeId")
        put(value: healthPlanName, key: "healthPlanName", label: labelHealthPlan)
        put(value: healthPlanId, key: "healthPlanId", label: labelHealthPlan)
        put(value: paymentMethodId, key: "paymentMethodId", label: labelPaymentMethod)
        put(value: specialityId, key: "specialityId", label: labelSpeciality)
        put(value: examTypeId, key: "examTypeId", label: labelExam)
        put(value: serviceTypeId, key: "serviceTypeId", label: labelServiceType)
        
        Util.showView(name: "MapPatientViewController", context: self)
    }
    
    func put(value: String, key: String, label: UILabel){
        if !label.isHidden {
            UserDefaults.standard.set(value, forKey: key)
        } else {
            UserDefaults.standard.set("", forKey: key)
        }
    }
    
}
