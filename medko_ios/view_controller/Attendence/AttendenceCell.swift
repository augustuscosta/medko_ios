//
//  AttendenceCell.swift
//  medko_ios
//
//  Created by Thialyson Martins on 28/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Cosmos

class AttendenceCell: UITableViewCell {

    @IBOutlet weak var avatarAttendenceList: UIImageView!
    @IBOutlet weak var nameAttendenceList: UILabel!
    @IBOutlet weak var statusAttendenceList: UILabel!
    @IBOutlet weak var examTypeAttendenceList: UILabel!
    @IBOutlet weak var specialityAttendenceList: UILabel!
    @IBOutlet weak var addressAttendenceList: UILabel!
    @IBOutlet weak var dateAttendenceList: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
