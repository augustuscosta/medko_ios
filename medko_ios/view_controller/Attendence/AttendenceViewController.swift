//
//  AttendenceViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 31/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Cosmos
import DropDown
import Alamofire
import AlamofireImage

class AttendenceViewController: UIViewController {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameAttendence: UILabel!
    @IBOutlet weak var examAttendence: UILabel!
    @IBOutlet weak var specialityAttendence: UILabel!
    @IBOutlet weak var telephone: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var dollarSign: UIImageView!
    @IBOutlet weak var localAttendence: UILabel!
    @IBOutlet weak var addressAttendence: UILabel!
    @IBOutlet weak var dateAttendence: UILabel!
    @IBOutlet weak var hourAttendence: UILabel!
    @IBOutlet weak var clock: UIImageView!
    @IBOutlet weak var separatorDate: UIView!
    @IBOutlet weak var buttonCancelAttendenceMarked: UIButton!
    @IBOutlet weak var buttonCancelAttendence: UIButton!
    @IBOutlet weak var buttonFinishAttendence: UIButton!
    @IBOutlet weak var buttonConfirm: UIButton!
    @IBOutlet weak var buttonPayment: UIButton!
    @IBOutlet weak var labelStatusAttendence: UILabel!
    @IBOutlet weak var labelRatingBar: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var ratingBarDoctor: CosmosView!
    @IBOutlet weak var buttonTakeTime: UIButton!
    @IBOutlet weak var viewTop: UIView!
    
    
    var correctNumberTelephone = ""
    var attendence: Attendence?
    var accontTypeCurrentUser = Int()
    var statusId = Int()
    var attDate: Date?
    let dropDown = DropDown()
    var takeTimeList = [String]()
    
    var dateChosen = ""
    var idDateChosen = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.getShadow(view: viewTop)
        accontTypeCurrentUser = UserDefaults.standard.integer(forKey: "accountTypeId")
        
        Request.getAttendece(id: AttendenceListViewController.attendenceId!) { (attendence, error) in
            if error == nil {
                self.attendence = attendence!
                self.statusId = attendence!.attendenceStatus!.id
                self.attDate = Util.getDate(dateStr: (attendence!.date.replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: ".000Z", with: "")))
                self.setComponents()
            } else {
                
            }
        }
    }
    
    func setComponents() {
        setAvatar()
        setCommomAttributes()
        setAddressAttendence()
        setPersonalAttributes()
        getStatusByAccountType()
        getStatusByAccountType()
    }
    
    func setPersonalAttributes() {
        switch accontTypeCurrentUser {
        case 1:
            nameAttendence.text = attendence!.doctor!.name
            correctNumberTelephone = attendence!.doctor!.telephone
            showRatingBarDoctor()
            break
        case 3:
            nameAttendence.text = attendence!.patient!.name
            correctNumberTelephone = attendence!.patient!.telephone
            break
        default:
            break
        }
    }
    
    func setAddressAttendence() {
        if attendence!.searchAddress?.address != "" {
            localAttendence.text = "Domicílio"
            addressAttendence.text = attendence!.searchAddress!.address
        } else {
            localAttendence.text = attendence!.place!.name
            addressAttendence.text = attendence!.place!.address
        }
    }
    
    func showRatingBarDoctor()  {
        if (attendence?.doctor?.rating)! && (attendence?.doctor?.media)! > 0 {
            ratingBarDoctor.settings.fillMode = .full
            ratingBarDoctor.isHidden = false
            ratingBarDoctor.rating = Double(attendence!.doctor!.media)
        }
    }
    
    func setAvatar(){
        let url = getUrl()
        Alamofire.request(url).responseImage { response in
            switch response.result{
            case .success:
                let size = CGSize(width: 50.0, height: 50.0)
                if let image = response.result.value?.af_imageAspectScaled(toFill: size).af_imageRoundedIntoCircle() {
                    self.avatar.image = image
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func getUrl() -> String {
        return accontTypeCurrentUser == 1 ? Util.root +  (attendence!.doctor!.avatar) : Util.root + (attendence!.patient!.avatar)
    }
    
    func setCommomAttributes() {
        setDate()
        labelStatusAttendence.text = attendence!.attendenceStatus!.name
        setExamType()
        setSpeciality()
        setValue()
    }
    
    func setExamType(){
        if attendence?.examType != nil {
            examAttendence.isHidden = false
            examAttendence.text = attendence!.examType!.name
        } else {
            examAttendence.isHidden = true
        }
    }
    
    func setSpeciality(){
        if attendence?.speciality != nil {
            examAttendence.isHidden = false
            specialityAttendence.text = attendence!.speciality!.name
        } else {
            examAttendence.isHidden = true
        }
    }
    
    func setValue(){
        if attendence?.healthPlan?.name != "" {
            price.text = attendence!.healthPlan!.name
        } else {
            if attendence!.value > 0.0  {
                price.text = String("R$ \(attendence!.value)")!.replacingOccurrences(of: ".", with: ",")
            }
        }
    }
    
    func setDate(){
        if attendence?.date != "" {
            showDateTime(date: attendence!.date)
        } else {
            showTakeTime()
        }
    }
    
    func showDateTime(date: String){
        clock.isHidden = false
        dateAttendence.isHidden = false
        hourAttendence.isHidden = false
        separatorDate.isHidden = false
        dateAttendence.text = Util.getOnlyDate(date: attDate!)
        hourAttendence.text = Util.getOnlyHours(date: attDate!)
    }
    
    func showTakeTime(){
        if accontTypeCurrentUser == 1 {
            buttonTakeTime.isHidden = false
            
            for index in 0..<attendence!.takeTimes.count {
                let dateStr = Util.getDateString(date: Util.getDate(dateStr: attendence!.takeTimes[index].date.replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: ".000Z", with: ""))!)
                
                takeTimeList.append(dateStr)
            }
        }
    }
    
    @IBAction func buttonTakeTimeClick(_ sender: Any) {
        let dropDown = getDropDown(dropDown: self.dropDown, anchor: buttonTakeTime , options: takeTimeList)
        dropDown.show()
    }
    
    
    func getStatusByAccountType() {
        if accontTypeCurrentUser > 0 && accontTypeCurrentUser == 1 {
            
            switch statusId {
            case 1:
                checkIfIsBeforeOrAfterAttendence()
                break
            case 2:
                checkIfAttendenceHasClassification()
                break
            case 3:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.5990125537, green: 0.3911141753, blue: 0.6621472836, alpha: 1))
                break
            case 4:
                paymentInvisible(paymentBool: false, cancelBool: false)
                break
            case 5:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.4316270351, green: 0.7640123963, blue: 0.6546236873, alpha: 1))
                break
            case 6:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1))
                break
            case 7:
                showComponentsByAttendenceTypeId()
                break
            default:
                break
            }
            
        } else if accontTypeCurrentUser > 0 && accontTypeCurrentUser == 3 {
            
            switch statusId {
            case 1:
                checkIfIsBeforeOrAfterAttendence()
                break
            case 2, 5:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.4316270351, green: 0.7640123963, blue: 0.6546236873, alpha: 1))
                break
            case 3:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.5990125537, green: 0.3911141753, blue: 0.6621472836, alpha: 1))
                break
            case 4, 7:
                setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1))
                break
            case 6:
                buttonConfirmInvisible(confirmBool: false, cancelBool: false)
                break
            default:
                break
            }
        }
    }
    
    func checkIfAttendenceHasClassification() {
        if (attendence?.classification?.value)! > 0 {
            setVisibilityStatus(invisibleBool: false, color: #colorLiteral(red: 0.4316270351, green: 0.7640123963, blue: 0.6546236873, alpha: 1))
        } else {
            ratingBar.isHidden = false
            labelRatingBar.isHidden = false
            
            ratingBar.didTouchCosmos = { rating in
                self.ratingBar.rating = rating
                self.sendClassification(value: Int(rating))
            }
        }
    }
    
    func sendClassification(value: Int) {
        let classification = ["classification": ["value" : "\(value)"],"attendence_id": "\(attendence!.id)"] as [String : Any]
        Request.changeAttendenceStatus(url: "\(Util.rootUrl)classification.json", params: classification) { (resultBool, error) in
            if error == nil {
                self.refresh()
            }
        }
    }
    
    func checkIfIsBeforeOrAfterAttendence() {
        if attendence?.date != "" {
            if Date() > self.attDate! {
                buttonFinishAttendence.isHidden = false
            } else {
                buttonCancelAttendenceMarked.isHidden = false
            }
        }
    }
    
    func setVisibilityStatus(invisibleBool: Bool, color: UIColor){
        labelStatusAttendence.isHidden = invisibleBool
        labelStatusAttendence.textColor = color
    }
    
    func paymentInvisible(paymentBool: Bool, cancelBool: Bool){
        buttonCancelAttendence.isHidden = paymentBool
        buttonPayment.isHidden = cancelBool
    }
    
    func buttonConfirmInvisible(confirmBool: Bool, cancelBool: Bool){
        buttonCancelAttendence.isHidden = cancelBool
        buttonConfirm.isHidden = confirmBool
    }
    
    func showComponentsByAttendenceTypeId() {
        if attendence?.attendenceType?.id == 1 {
            paymentInvisible(paymentBool: false, cancelBool: false)
        } else if attendence?.attendenceType?.id == 2 {
            buttonTakeTime.isHidden = false
            buttonConfirmInvisible(confirmBool: false, cancelBool: false)
        }
    }
    
    func hideComponents(){
        paymentInvisible(paymentBool: true, cancelBool: true)
        buttonConfirmInvisible(confirmBool: true, cancelBool: true)
        buttonFinishAttendence.isHidden = true
        buttonCancelAttendenceMarked.isHidden = true
        ratingBar.isHidden = true
        labelRatingBar.isHidden = true
        buttonTakeTime.isHidden = true
    }
    
    func refresh() {
        self.hideComponents()
        viewDidLoad()
    }
    
    @IBAction func finishAttendenceClick(_ sender: Any) {
        finishAttendence()
    }
    
    func finishAttendence() {
        let url = "\(Util.rootUrl)attendences/confirm.json"
        let params = ["attendence":["id": "\(attendence!.id)"], "date" : ""] as [String : Any]
        Request.changeAttendenceStatus(url: url, params: params, handler: { (resultBool, error) in
            if error == nil {
                if resultBool {
                    self.refresh()
                }
            } else {
                print("Erro na requisicao")
            }
        })
    }
    
    @IBAction func cancelMarkedButtonClick(_ sender: Any) {
        let url = "\(Util.rootUrl)attendences/cancel.json"
        let params = ["attendence":["id": "\(attendence!.id)"], "date" : ""] as [String : Any]
        Request.changeAttendenceStatus(url: url, params: params, handler: { (resultBool, error) in
            if error == nil {
                if resultBool {
                    if self.accontTypeCurrentUser == 1 {
                        Util.showAlert(alert: Util.getAlert(title: "Cancelamentos Sucessivos:", message: "Cancelamentos sucessivos podem bloquear a sua conta!"), context: self)
                        self.refresh()
                    } else {
                        self.refresh()
                    }
                }
            }
        })
    }
    
    @IBAction func buttonConfirmClick(_ sender: Any) {
        if accontTypeCurrentUser == 1 {
            
            if attendence != nil && dateChosen != "" {
                let url = "\(Util.rootUrl)attendences/time.json"
                let params = ["attendence":["id": "\(attendence!.id)", "date" : dateChosen]] as [String : Any]
                
                Request.changeAttendenceStatus(url: url, params: params, handler: { (resultBool, error) in
                    if error == nil {
                        if resultBool {
                            self.refresh()
                        }
                    } else {
                        print("debug")
                    }
                })
            }
        } else {
            finishAttendence()
        }
    }
    
    
    @IBAction func buttonPaymentClick(_ sender: Any) {
        Request.requestPayment(id: attendence!.id) { (paymentRequest, error) in
            if error == nil {
                PagSeguroViewController.url = paymentRequest!.url
                Util.showView(name: "PagSeguroViewController", context: self)
            }
        }
    }
    
    func getDropDown(dropDown: DropDown, anchor: UIButton, options: [String]) -> DropDown {
        dropDown.anchorView = anchor
        dropDown.dataSource = options
        dropDown.selectionAction = { (index: Int, item: String) in
            
            anchor.setTitle(item, for: UIControlState.normal)
            self.idDateChosen = index
            self.dateChosen = self.attendence!.takeTimes[index].date
        }
        return dropDown
    }
    
    
    @IBAction func buttonNavigationClick(_ sender: Any) {
        let latitude = attendence!.place!.latitude
        let longitude = attendence!.place!.longitude
        let urlstr = "waze://?ll=\(latitude),\(longitude)&navigate=yes"
        let url = URL(string: urlstr)
        let itunesUrl = URL(string: "http://itunes.apple.com/us/app/id323229106")
        
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:])
        } else if UIApplication.shared.canOpenURL(itunesUrl!) {
            UIApplication.shared.open(itunesUrl!, options: [:])
        }
        
    }
    
    @IBAction func buttonTelephoneClick(_ sender: Any) {
        
//        let tel = attendence!.doctor!.telephone
        let tel = "85988183444"
        
        if let number = URL(string: "telprompt://\(tel)"){
            if UIApplication.shared.canOpenURL(number) {
                UIApplication.shared.open(number as URL, options: [:])
            }
        } else {
            print("Não deu certo!")
        }
    }
    
}
