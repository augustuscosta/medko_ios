//
//  RecoverPassViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 11/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit
import Alamofire

class RecoverPassViewController: UIViewController {
    
    @IBOutlet weak var emailPasswordRecover: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func validateField(_ sender: Any) {
        var list: [Bool] = []
        list.append(Validate.check(field: emailPasswordRecover.text!, min: 1, max: 100, name: "Email", context: self))
        
        if Validate.checkIfAllAreTrue(list: list){
            recover()
        }
    }
    
    func recover() {
        let params = ["email": emailPasswordRecover.text!]
        let url = "\(Util.rootUrl)/users/reset_password"
        Alamofire.request(url, method: .post, parameters: params).response { response in
            
            if response.response?.statusCode == 200 {
                self.present(Util.getAlert(title: "Sucesso", message: "Foi enviado para o seu email informações de como recuperar sua senha!"), animated: true, completion: nil)
                
            } else {
                self.present(Util.getAlert(title: "Email Não Encotrado", message: "Email não encontrado na nossa base!"), animated: true, completion: nil)
            }
        }
    }
}
