//
//  CustomCell.swift
//  medko_ios
//
//  Created by Thialyson Martins on 13/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
