//
//  TableViewController.swift
//  medko_ios
//
//  Created by Thialyson Martins on 17/01/17.
//  Copyright © 2017 On The Go Mobile. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var titleList: UITextView!
    @IBOutlet weak var tableView: UITableView!
    var names = RegisterDoctorViewController.namesArray
    var key = RegisterDoctorViewController.key
    
    var specialityList = [Speciality]()
    var examTypeList = [ExamType]()
    
    static var selectedSpecialities = [String]()
    static var selectedExamTypes = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleList.text = RegisterDoctorViewController.titleList
        specialityList = (!(Speciality.getAll()?.isEmpty)! ? Speciality.getAll() : nil)!
        examTypeList = (!(ExamType.getAll()?.isEmpty)! ? ExamType.getAll() : nil)!
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReused", for: indexPath)
        cell.textLabel?.text = names[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
        
        if key == 1 {
            if !TableViewController.selectedSpecialities.contains(String(self.specialityList[indexPath.row].id)) {
                TableViewController.selectedSpecialities.append(String(self.specialityList[indexPath.row].id))
            }
            
            print("Especialidades: ", TableViewController.selectedSpecialities)
            
        } else if key == 2 {
            
            if !TableViewController.selectedExamTypes.contains(String(self.examTypeList[indexPath.row].id)) {
                TableViewController.selectedExamTypes.append(String(self.examTypeList[indexPath.row].id))
            }
            
            print("Exames: ", TableViewController.selectedExamTypes)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .none
        
        if key == 1 {
            if TableViewController.selectedSpecialities.contains(String(self.specialityList[indexPath.row].id)) {
                if let index = TableViewController.selectedSpecialities.index(of: String(self.specialityList[indexPath.row].id)) {
                    TableViewController.selectedSpecialities.remove(at: index)
                }
            }
            print("Especialidades: ", TableViewController.selectedSpecialities)
        } else if key == 2 {
            if TableViewController.selectedExamTypes.contains(String(self.examTypeList[indexPath.row].id)) {
                if let index = TableViewController.selectedExamTypes.index(of: String(self.examTypeList[indexPath.row].id)) {
                    TableViewController.selectedExamTypes.remove(at: index)
                }
                print("Exames: ", TableViewController.selectedExamTypes)
            }
        }
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
